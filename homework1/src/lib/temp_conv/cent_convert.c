/*---------------------------------------------------------------------
 *
 * cent_convert.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Makefiles Homework 1
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */

/* User */
#include "cent_convert.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

// Move this to include directory.
// double convert_to_cent(double fahr);

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: Fahrenheit decimal value.
 * Outputs: Centigrade value. 
 * Note: All values should be in double format.
 * 
----------------------------------------------------------------------*/

double convert_to_cent(double fahr)
{
    double celsius = 0.0;
    celsius = (fahr - 32.0) * (5.0 / 9.0);

    return celsius;
}

