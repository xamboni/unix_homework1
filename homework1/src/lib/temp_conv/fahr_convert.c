
/*---------------------------------------------------------------------
 *
 * fahr_convert.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Makefiles Homework 1
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */

/* User */
#include "fahr_convert.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

// Moved to include file.
// double convert_to_fahr(double cent);

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: Centigrade decimal value.
 * Outputs: Fahrenheit value. 
 * Note: All values should be in double format.
 * 
----------------------------------------------------------------------*/

double convert_to_fahr(double cent)
{
    double fahrenheit = 0.0;
    fahrenheit = (cent * (5.0 / 9.0)) + 32.0;

    return fahrenheit;
}
