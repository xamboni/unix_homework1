
/*---------------------------------------------------------------------
 *
 * getarg.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Makefiles Homework 1
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <stdlib.h>

/* User */
#include "getarg.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

// Moved to the include directory.
//int get_argument(int argc, char* argv[], double* return_value);

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs:
 * Outputs:
 * Note:
 * 
----------------------------------------------------------------------*/

int get_argument(int argc, char* argv[], double* return_value)
{
    // If there is an argv[1] place value of atof(argv[1]) into double pointed to by return_value.
    if ( argc > 1 )
    {
        // NOTE: This does not handle errors returned by atof().
        printf("Arg: %s\n", argv[1]);
        *return_value = atof( argv[1] );
        printf("Atof: %f\n", *return_value);
    }

    else if (argc <= 1)
    {
        char str1[20];
 
        printf("Enter number of centigrade degrees (less than 20 characters please): ");
        scanf("%s", str1);
        *return_value = atof( str1 );
    }
    // NOTE: This is a catch all to satisfy instructions.
    else
    {
        int ERROR = -1;

        return ERROR;
    }
}
