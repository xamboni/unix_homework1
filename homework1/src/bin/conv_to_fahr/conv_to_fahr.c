/*---------------------------------------------------------------------
 *
 * conv_to_fahr.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Makefiles Homework 1
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <stdlib.h>

/* User */
#include "getarg.h"
#include "fahr_convert.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: 
 * Outputs:
 * Note: 
 * 
----------------------------------------------------------------------*/

int main ( int argc, char* argv[] )
{

        double fahrenheit = 0.0;
        double celsius = 0.0;

        get_argument(argc, argv, &celsius);
        printf("Cel value: %f.\n", celsius);
      
        fahrenheit = convert_to_fahr ( celsius );

        printf("Fahrenheit value: %f.\n", fahrenheit);
}
