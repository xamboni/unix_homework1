/*---------------------------------------------------------------------
 *
 * conv_to_cent.c
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Unix Programming Fall 2017
 * Makefiles Homework 1
 * 
 *
 ---------------------------------------------------------------------*/

/*---------------------------- Includes ------------------------------*/

/* Standard */
#include <stdio.h>
#include <string.h>

/* User */
#include "getarg.h"
#include "cent_convert.h"

/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/

/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: 
 * Outputs:
 * Note: 
 * 
----------------------------------------------------------------------*/

int main ( int argc, char* argv[] )
{

    if ( argc > 1 )
    {
        double fahrenheit = 0.0;
        double celsius = 0.0;

        get_argument(argc, argv, &fahrenheit);
        
        // Check if atof was a success.
        // NOTE: This only handles "0".
        if ( fahrenheit == 0 && strcmp(argv[1], "0") )
        {
            printf("Invalid Fahrenheit input.\n");
        }
        
        // Things went ok with atof check.
        else
        {
            celsius = convert_to_cent( fahrenheit );
            printf("Centigrade value: %f.\n", celsius);
        }
    }

    // Catch all here.
    else
    {
        printf("Please supply an input to be converted.\n");
    }
}
