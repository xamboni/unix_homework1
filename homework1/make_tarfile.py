import tarfile
import os
import subprocess
from optparse import OptionParser

def make_tarfile(output_filename, source_dir, base):
    with tarfile.open(output_filename, "w:gz") as tar:
        if base:
            tar.add(source_dir, arcname=os.path.basename(source_dir))
        elif not base:
            tar.add(source_dir, "homework1/bin")
        else:
            print "Error while tar'ing."

source_dir = "homework1"
output_filename = "homework1.tar"

parser = OptionParser()
parser.add_option("-b", "--binary", action="store_true", dest="binary",
                  help="Request binary release.")
parser.add_option("-s", "--source", action="store_true", dest="source",
                  help="Request source release.")

(options, args) = parser.parse_args()

if options.binary:
    var = raw_input("Are you sure you would like to request a binary release? (Y/N): ")
    if var == "Y" or var == "y":
        hostname = raw_input("Tar'ing binary release for homework1. Please enter a hostname: ")
        output_filename = "homework1_" + hostname + ".tar"
        source_dir = "../homework1/bin"
        make_tarfile(output_filename, source_dir, False)
    elif var == "N" or var == "n":
        print "you entered" + var
    else:
        print "Incorrect input"

elif options.source:
    var = raw_input("Are you sure you would like to request a source release? (Y/N): ")
    if var == "Y" or var == "y":
        print "Tar'ing source release for homework1."
        make_process = subprocess.Popen("make clean", stderr=subprocess.STDOUT, shell=True)
        if make_process.wait() != 0:
            print "Something went wrong while executing `make clean`."
        source_dir = "../homework1"
        make_tarfile(output_filename, source_dir, True)
    elif var == "N" or var == "n":
        print "you entered" + var
    else:
        print "Incorrect input"

else:
    print "You must specify a binary (-b, --binary) or source (-s, --source) release."

